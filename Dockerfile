FROM maven:3-jdk-8-alpine AS build
WORKDIR /usr/src/app
COPY . /usr/src/app
RUN mvn package

ENV PORT 5000
EXPOSE $PORT
CMD [ "sh", "-c", "mvn -Dserver.port=${PORT} spring-boot:run" ]

FROM openjdk:8-jdk-alpine
ARG JAR_FILE=/usr/src/app/target/*.jar 
COPY --from=build ${JAR_FILE} app.jar 
ENTRYPOINT ["java","-jar", "/app.jar"]


